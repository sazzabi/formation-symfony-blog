<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220120141626 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE article ADD created_by_id INT DEFAULT NULL, ADD updated_by_id INT DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE article ADD CONSTRAINT FK_23A0E66B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)'
        );
        $this->addSql(
            'ALTER TABLE article ADD CONSTRAINT FK_23A0E66896DBBDE FOREIGN KEY (updated_by_id) REFERENCES user (id)'
        );
        $this->addSql('CREATE INDEX IDX_23A0E66B03A8386 ON article (created_by_id)');
        $this->addSql('CREATE INDEX IDX_23A0E66896DBBDE ON article (updated_by_id)');
        $this->addSql(
            'ALTER TABLE category ADD created_by_id INT DEFAULT NULL, ADD updated_by_id INT DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE category ADD CONSTRAINT FK_64C19C1B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)'
        );
        $this->addSql(
            'ALTER TABLE category ADD CONSTRAINT FK_64C19C1896DBBDE FOREIGN KEY (updated_by_id) REFERENCES user (id)'
        );
        $this->addSql('CREATE INDEX IDX_64C19C1B03A8386 ON category (created_by_id)');
        $this->addSql('CREATE INDEX IDX_64C19C1896DBBDE ON category (updated_by_id)');
        $this->addSql(
            'ALTER TABLE groups ADD created_by_id INT DEFAULT NULL, ADD updated_by_id INT DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE groups ADD CONSTRAINT FK_F06D3970B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)'
        );
        $this->addSql(
            'ALTER TABLE groups ADD CONSTRAINT FK_F06D3970896DBBDE FOREIGN KEY (updated_by_id) REFERENCES user (id)'
        );
        $this->addSql('CREATE INDEX IDX_F06D3970B03A8386 ON groups (created_by_id)');
        $this->addSql('CREATE INDEX IDX_F06D3970896DBBDE ON groups (updated_by_id)');
        $this->addSql(
            'ALTER TABLE role ADD created_by_id INT DEFAULT NULL, ADD updated_by_id INT DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE role ADD CONSTRAINT FK_57698A6AB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)'
        );
        $this->addSql(
            'ALTER TABLE role ADD CONSTRAINT FK_57698A6A896DBBDE FOREIGN KEY (updated_by_id) REFERENCES user (id)'
        );
        $this->addSql('CREATE INDEX IDX_57698A6AB03A8386 ON role (created_by_id)');
        $this->addSql('CREATE INDEX IDX_57698A6A896DBBDE ON role (updated_by_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66B03A8386');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66896DBBDE');
        $this->addSql('DROP INDEX IDX_23A0E66B03A8386 ON article');
        $this->addSql('DROP INDEX IDX_23A0E66896DBBDE ON article');
        $this->addSql('ALTER TABLE article DROP created_by_id, DROP updated_by_id, DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1B03A8386');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1896DBBDE');
        $this->addSql('DROP INDEX IDX_64C19C1B03A8386 ON category');
        $this->addSql('DROP INDEX IDX_64C19C1896DBBDE ON category');
        $this->addSql('ALTER TABLE category DROP created_by_id, DROP updated_by_id, DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE groups DROP FOREIGN KEY FK_F06D3970B03A8386');
        $this->addSql('ALTER TABLE groups DROP FOREIGN KEY FK_F06D3970896DBBDE');
        $this->addSql('DROP INDEX IDX_F06D3970B03A8386 ON groups');
        $this->addSql('DROP INDEX IDX_F06D3970896DBBDE ON groups');
        $this->addSql('ALTER TABLE groups DROP created_by_id, DROP updated_by_id, DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE role DROP FOREIGN KEY FK_57698A6AB03A8386');
        $this->addSql('ALTER TABLE role DROP FOREIGN KEY FK_57698A6A896DBBDE');
        $this->addSql('DROP INDEX IDX_57698A6AB03A8386 ON role');
        $this->addSql('DROP INDEX IDX_57698A6A896DBBDE ON role');
        $this->addSql('ALTER TABLE role DROP created_by_id, DROP updated_by_id, DROP created_at, DROP updated_at');
    }
}
