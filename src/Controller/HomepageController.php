<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Security\MaClasse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    #[Route('/{page}', name: 'homepage', requirements: ['page' => '\d+'])]
    public function index(ArticleRepository $repository, int $page = 1): Response
    {
        $articles = $repository->findForHomepage($page);
        $total = count($articles);
        $hasNext = ($page * Article::PAGE_SIZE) <= $total;

        return $this->render('homepage/index.html.twig', [
            'articles' => $articles,
            'currentPage' => $page,
            'hasPrevious' => $page > 1,
            'hasNext' => $hasNext,
        ]);
    }

    #[Route('/{id}-{slug}', name: 'detail', requirements: ['id' => '\d+'])]
    public function detail(ArticleRepository $repository, int $id, string $slug): Response
    {
        $article = $repository->findArticle($id, $slug);

        if (!$article) {
            throw  new NotFoundHttpException('Article not found');
        }

        return $this->render('homepage/detail.html.twig', [
            'article' => $article,
        ]);
    }
}
