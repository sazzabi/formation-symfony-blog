<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Group;
use App\Entity\Role;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private AdminUrlGenerator $routeBuilder)
    {
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
                        ->setTitle('Blog');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('Blog');
        // yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Articles', 'fas fa-list', Article::class);
        yield MenuItem::linkToCrud('Categories', 'fas fa-sitemap', Category::class);
        yield MenuItem::section('Parameters');
        yield MenuItem::subMenu('Security', 'fas fa-shield-alt')->setSubItems([
            MenuItem::linkToCrud('Users', '', User::class),
            MenuItem::linkToCrud('Groups', '', Group::class),
            MenuItem::linkToCrud('Roles', '', Role::class),
        ]);
    }

    public function configureUserMenu(User|UserInterface $user): UserMenu
    {
        return parent::configureUserMenu($user)
                     ->setName($user->getFirstName().' '.$user->getLastName());
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $url = $this->routeBuilder->setController(ArticleCrudController::class)->generateUrl();

        return $this->redirect($url);
    }
}
