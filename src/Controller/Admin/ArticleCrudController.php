<?php

namespace App\Controller\Admin;

use App\Component\Publication\ArticlePublicationRegistry;
use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\BooleanFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\DateTimeFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use Symfony\Component\HttpFoundation\Response;

class ArticleCrudController extends AbstractCrudController
{
    public function __construct(
        private EntityManagerInterface $em,
        private ArticlePublicationRegistry $registry
    ) {
    }

    public function configureActions(Actions $actions): Actions
    {
        $draftAction = Action::new('draft', 'Draft', 'fas fa-edit text-warning')
                             ->linkToCrudAction('draft');
        $reviewtAction = Action::new('review', 'Review', 'fas fa-hourglass-half text-primary')
                               ->linkToCrudAction('review');
        $rejectAction = Action::new('reject', 'Reject', 'fas fa-ban text-danger')
                              ->linkToCrudAction('reject');
        $publishAction = Action::new('publish', 'Publish', 'fas fa-paper-plane text-success')
                               ->linkToCrudAction('publish');
        $archiveAction = Action::new('archive', 'Archive', 'fas fa-archive text-dark')
                               ->linkToCrudAction('archive');

        return parent::configureActions($actions)
                     ->update(
                         Crud::PAGE_INDEX,
                         Action::NEW,
                         fn(Action $action) => $action->setIcon('fas fa-plus-circle')->setLabel('Add New')
                     )
                     ->add(Crud::PAGE_INDEX, $draftAction)
                     ->add(Crud::PAGE_INDEX, $reviewtAction)
                     ->add(Crud::PAGE_INDEX, $rejectAction)
                     ->add(Crud::PAGE_INDEX, $publishAction)
                     ->add(Crud::PAGE_INDEX, $archiveAction)
                     ->setPermission(Action::NEW, 'ROLE_ARTICLE_CREATE')
                     ->setPermission(Action::EDIT, 'edit')
                     ->setPermission('draft', 'draft')
                     ->setPermission('review', 'review')
                     ->setPermission('reject', 'reject')
                     ->setPermission('publish', 'publish')
                     ->setPermission('archive', 'archive');
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
                     ->setPageTitle(Crud::PAGE_INDEX, 'Articles list')
                     ->setPageTitle(Crud::PAGE_NEW, 'Add new article')
                     ->setPageTitle(Crud::PAGE_EDIT, 'Edit article');
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addTab('Article details');
        yield IdField::new('id')
                     ->hideOnForm();
        yield TextField::new('title')
                       ->setColumns(9);
        yield TextField::new('slug')
                       ->setRequired(false)
                       ->setColumns(9);
        yield TextEditorField::new('body')
                             ->setColumns(9)
                             ->hideOnIndex();

        yield FormField::addTab('Publication', 'fas fa-paper-plane');
        yield AssociationField::new('category')
                              ->setRequired(true)
                              ->setColumns(6);
        yield FormField::addRow();

        yield ChoiceField::new('status')
                         ->setColumns(3)
                         ->setChoices([
                             'Draft' => Article::ARTICLE_STATUS_DRAFT,
                             'To review' => Article::ARTICLE_STATUS_TO_REVIEW,
                             'Rejected' => Article::ARTICLE_STATUS_REJECTED,
                             'Published' => Article::ARTICLE_STATUS_PUBLISHED,
                             'Archived' => Article::ARTICLE_STATUS_ARCHIVED,
                         ])
                         ->setTemplatePath('crud/fields/status.html.twig');
        yield DateTimeField::new('publishedAt')
                           ->setColumns(3);
        yield FormField::addRow();

        yield BooleanField::new('public')
                          ->setColumns(3);
    }

    public function configureFilters(Filters $filters): Filters
    {
        return parent::configureFilters($filters)
                     ->add(EntityFilter::new('category'))
                     ->add(DateTimeFilter::new('publishedAt', 'Published At'))
                     ->add(BooleanFilter::new('public'));
    }

    public static function getEntityFqcn(): string
    {
        return Article::class;
    }

    public function draft(AdminContext $context): Response
    {
        return $this->setStatus($context, Article::ARTICLE_STATUS_DRAFT);
    }

    private function setStatus(AdminContext $context, string $status): Response
    {
        /** @var Article $article */
        $article = $context->getEntity()->getInstance();
        $article = $this->registry->handle($article, $status);

        $this->em->flush();

        return $this->redirect($context->getReferrer());
    }

    public function review(AdminContext $context): Response
    {
        return $this->setStatus($context, Article::ARTICLE_STATUS_TO_REVIEW);
    }

    public function reject(AdminContext $context): Response
    {
        return $this->setStatus($context, Article::ARTICLE_STATUS_REJECTED);
    }

    public function publish(AdminContext $context): Response
    {
        return $this->setStatus($context, Article::ARTICLE_STATUS_PUBLISHED);
    }

    public function archive(AdminContext $context): Response
    {
        return $this->setStatus($context, Article::ARTICLE_STATUS_ARCHIVED);
    }
}
