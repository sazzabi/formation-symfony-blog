<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CategoryCrudController extends AbstractCrudController
{
    public function configureActions(Actions $actions): Actions
    {
        return parent::configureActions($actions)
                     ->update(
                         Crud::PAGE_INDEX,
                         Action::NEW,
                         fn(Action $action) => $action->setIcon('fas fa-plus-circle')->setLabel('Add New')
                     );
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
                     ->setPageTitle(Crud::PAGE_INDEX, 'Categories list')
                     ->setPageTitle(Crud::PAGE_NEW, 'Add new category')
                     ->setPageTitle(Crud::PAGE_EDIT, 'Edit category')
                     ->showEntityActionsInlined(true);
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')
                     ->hideOnForm();
        yield TextField::new('name');
        yield TextField::new('slug')
                       ->setRequired(false);
    }

    public static function getEntityFqcn(): string
    {
        return Category::class;
    }
}
