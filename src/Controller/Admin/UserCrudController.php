<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Security\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Response;

class UserCrudController extends AbstractCrudController
{
    public function __construct(private UserManager $userManager)
    {
    }

    public function configureActions(Actions $actions): Actions
    {
        $sendEmailConfirmation = Action::new('sendEmailConfirmation', 'Send Email Confirmation', 'fas fa-envelope')
                                       ->linkToCrudAction('sendEmailConfirmation');

        return parent::configureActions($actions)
                     ->add(Crud::PAGE_INDEX, $sendEmailConfirmation)
                     ->setPermission('sendEmailConfirmation', 'sendEmailConfirmation')
                     ->add(Crud::PAGE_INDEX, Action::DETAIL)
                     ->update(
                         Crud::PAGE_INDEX,
                         Action::NEW,
                         fn(Action $action) => $action->setIcon('fas fa-plus-circle')->setLabel('Add New')
                     );
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
                     ->setPageTitle(Crud::PAGE_INDEX, 'Users list')
                     ->setPageTitle(Crud::PAGE_NEW, 'Add new user')
                     ->setPageTitle(Crud::PAGE_EDIT, 'Edit user');
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')
                     ->hideOnForm();
        yield TextField::new('firstName');
        yield TextField::new('lastName');
        yield EmailField::new('email');
        yield TextField::new('oldPassword')
                       ->setFormType(PasswordType::class)
                       ->setFormTypeOption('attr', ['autocomplete' => 'new-password'])
                       ->setFormTypeOption('empty_data', '')
                       ->setRequired(false)
                       ->onlyWhenUpdating();
        yield TextField::new('newPassword')
                       ->setFormType(PasswordType::class)
                       ->setFormTypeOption('attr', ['autocomplete' => 'new-password'])
                       ->setFormTypeOption('empty_data', '')
                       ->setRequired(false)
                       ->onlyOnForms();
        yield TextField::new('newPasswordRepeat')
                       ->setFormType(PasswordType::class)
                       ->setFormTypeOption('attr', ['autocomplete' => 'new-password'])
                       ->setFormTypeOption('empty_data', '')
                       ->setRequired(false)
                       ->onlyOnForms();
        yield AssociationField::new('group');
        yield BooleanField::new('isVerified')
                          ->setDisabled(true);
        yield BooleanField::new('isEnabled');
    }

    public function configureFilters(Filters $filters): Filters
    {
        return parent::configureFilters($filters)
                     ->add(EntityFilter::new('groups'));
    }

    public function createEntity(string $entityFqcn)
    {
        return (new User())->setPassword('');
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->changePassword($entityInstance);
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    protected function changePassword(User $user): void
    {
        if ($user->getNewPassword() !== '' && $user->getNewPasswordRepeat() !== '') {
            $this->userManager->changePassword($user);
        }

        if (!$user->isVerified()) {
            $this->userManager->sendEmailConfirmation($user);
        }
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->changePassword($entityInstance);
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function sendEmailConfirmation(AdminContext $context): Response
    {
        $this->userManager->sendEmailConfirmation($context->getEntity()->getInstance());

        return $this->redirect($context->getReferrer());
    }
}
