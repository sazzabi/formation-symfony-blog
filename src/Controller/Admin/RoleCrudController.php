<?php

namespace App\Controller\Admin;

use App\Entity\Role;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

class RoleCrudController extends AbstractCrudController
{
    public function configureActions(Actions $actions): Actions
    {
        return parent::configureActions($actions)
                     ->update(
                         Crud::PAGE_INDEX,
                         Action::NEW,
                         fn(Action $action) => $action->setIcon('fas fa-plus-circle')->setLabel('Add New')
                     );
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
                     ->setPageTitle(Crud::PAGE_INDEX, 'Roles list')
                     ->setPageTitle(Crud::PAGE_NEW, 'Add new role')
                     ->setPageTitle(Crud::PAGE_EDIT, 'Edit role')
                     ->setDefaultSort(['id' => 'ASC'])
                     ->showEntityActionsInlined(true);
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')
                     ->hideOnForm();
        yield TextField::new('name');
        yield AssociationField::new('groups')
                              ->setFormTypeOption('by_reference', false);
    }

    public function configureFilters(Filters $filters): Filters
    {
        return parent::configureFilters($filters)
                     ->add(EntityFilter::new('groups'));
    }

    public static function getEntityFqcn(): string
    {
        return Role::class;
    }
}
