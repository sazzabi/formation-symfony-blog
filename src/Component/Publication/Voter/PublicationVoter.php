<?php

namespace App\Component\Publication\Voter;

use App\Entity\Article;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PublicationVoter extends Voter
{
    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, ['draft', 'review', 'reject', 'publish', 'archive'])
               && $subject instanceof Article;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var Article $subject */
        return match ($attribute) {
            'draft' => $subject->getStatus() !== Article::ARTICLE_STATUS_DRAFT,
            'review' => $subject->getStatus() === Article::ARTICLE_STATUS_DRAFT,
            'reject', 'publish' => $subject->getStatus() === Article::ARTICLE_STATUS_TO_REVIEW,
            'archive' => ($subject->getStatus() === Article::ARTICLE_STATUS_PUBLISHED ||
                          $subject->getStatus() === Article::ARTICLE_STATUS_REJECTED),
            default => false,
        };
    }
}
