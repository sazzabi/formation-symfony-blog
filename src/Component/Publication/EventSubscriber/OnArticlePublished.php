<?php

namespace App\Component\Publication\EventSubscriber;

use App\Component\Publication\Event\PublicationEvent;
use App\Entity\Article;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;

class OnArticlePublished implements EventSubscriberInterface
{
    public function __construct(private MailerInterface $mailer)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            Article::ARTICLE_STATUS_PUBLISHED => 'onArticlePublished',
            Article::ARTICLE_STATUS_TO_REVIEW => 'onArticleToReview',
        ];
    }

    public function onArticlePublished(PublicationEvent $event): void
    {
        // $email = (new Email())
        //     ->from(new Address('contact@cicero-crm.com', 'Saifallah Azzabi'))
        //     ->to('seifallah.azzabi@gmail.com')
        //     ->subject('New article published on your blog')
        //     ->text('a new article have been published on your blog: '.$event->getArticle()->getTitle());
        //
        // $this->mailer->send($email);
    }

    public function onArticleToReview(PublicationEvent $event): void
    {
        // $email = (new Email())
        //     ->from(new Address('contact@cicero-crm.com', 'Saifallah Azzabi'))
        //     ->to('seifallah.azzabi@gmail.com')
        //     ->subject('New article must be reviewd on your blog')
        //     ->text('a new article must be reviewd on your blog');
        //
        // $this->mailer->send($email);
    }
}

