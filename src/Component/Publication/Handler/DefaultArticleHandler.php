<?php

namespace App\Component\Publication\Handler;

use App\Entity\Article;

class DefaultArticleHandler implements PublicationHandlerInterface
{
    public function supports(string $status): bool
    {
        return in_array($status, [
            Article::ARTICLE_STATUS_DRAFT,
            Article::ARTICLE_STATUS_ARCHIVED,
            Article::ARTICLE_STATUS_REJECTED,
            Article::ARTICLE_STATUS_TO_REVIEW,
        ], true);
    }

    public function handle(Article $article, string $status): Article
    {
        $article->setStatus($status);

        return $article;
    }
}
