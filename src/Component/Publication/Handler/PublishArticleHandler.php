<?php

namespace App\Component\Publication\Handler;

use App\Entity\Article;
use DateTime;

class PublishArticleHandler implements PublicationHandlerInterface
{
    public function supports(string $status): bool
    {
        return $status === Article::ARTICLE_STATUS_PUBLISHED;
    }

    public function handle(Article $article, string $status): Article
    {
        $article->setStatus($status)
                ->setPublishedAt(new DateTime());

        return $article;
    }
}
