<?php

namespace App\Component\Publication\Handler;

use App\Entity\Article;

interface PublicationHandlerInterface
{
    public function supports(string $status): bool;

    public function handle(Article $article, string $status): Article;
}
