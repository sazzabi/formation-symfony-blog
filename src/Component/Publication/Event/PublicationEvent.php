<?php

namespace App\Component\Publication\Event;

use App\Entity\Article;
use Symfony\Contracts\EventDispatcher\Event;

class PublicationEvent extends Event
{
    protected Article $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function getArticle(): Article
    {
        return $this->article;
    }
}
