<?php

namespace App\Component\Publication;

use App\Component\Publication\Event\PublicationEvent;
use App\Component\Publication\Handler\PublicationHandlerInterface;
use App\Entity\Article;
use Psr\EventDispatcher\EventDispatcherInterface;

class ArticlePublicationRegistry
{
    /** @var array<PublicationHandlerInterface> */
    private array $handlers;

    public function __construct(iterable $handlers, private EventDispatcherInterface $dispatcher)
    {
        $this->handlers = iterator_to_array($handlers);
    }

    public function handle(Article $article, string $status)
    {
        foreach ($this->handlers as $handler) {
            if ($handler->supports($status)) {
                $article = $handler->handle($article, $status);

                $event = new PublicationEvent($article);
                $this->dispatcher->dispatch($event, $status);

                return $article;
            }
        }
    }
}
