<?php

namespace App\Security;

use App\Entity\User;
use RuntimeException;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserManager
{
    public function __construct(
        private UserPasswordHasherInterface $userPasswordHasher,
        private RequestStack                $requestStack,
        private EmailVerifier               $emailVerifier
    ) {
    }

    /**
     * @throws RuntimeException
     */
    public function changePassword(User $user): User
    {
        if ($user->getNewPassword() === '' && $user->getNewPasswordRepeat() === '') {
            return $user;
        }

        if ($user->getOldPassword() !== '' &&
            !$this->userPasswordHasher->isPasswordValid($user, $user->getOldPassword())) {
            throw new RuntimeException('Wrong old password');
        }

        $user->setPassword($this->userPasswordHasher->hashPassword($user, $user->getNewPassword()));
        $this->requestStack->getSession()->getFlashBag()->add('success', 'Password changed');

        return $user;
    }

    public function sendEmailConfirmation(User $user): void
    {
        $this->emailVerifier->sendEmailConfirmation(
            'app_verify_email',
            $user,
            (new TemplatedEmail())
                ->from(new Address('contact@cicero-crm.com', 'Cicero Blog'))
                ->to($user->getEmail())
                ->subject('Please Confirm your Email')
                ->htmlTemplate('security/confirmation_email.html.twig')
        );
    }
}
