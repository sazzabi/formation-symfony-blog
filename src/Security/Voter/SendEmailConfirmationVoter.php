<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SendEmailConfirmationVoter extends Voter
{
    protected function supports(string $attribute, $subject): bool
    {
        return $attribute === 'sendEmailConfirmation' && $subject instanceof User;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $subject */

        return !$subject->isVerified();
    }
}
