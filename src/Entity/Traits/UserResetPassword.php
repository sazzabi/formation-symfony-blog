<?php

namespace App\Entity\Traits;

use App\Validator as AppAssert;
use Symfony\Component\Validator\Constraints as Assert;

trait UserResetPassword
{
    #[AppAssert\OldPassword]
    #[Assert\Expression(
        "this.getOldPassword() === '' || this.getOldPassword() !== this.getNewPassword()",
        message: 'Old and new passwords must not be the same.'
    )]
    private string $oldPassword = '';

    #[AppAssert\MinOrEmpty(length: 6)]
    #[Assert\Expression(
        "this.getNewPassword() === this.getNewPasswordRepeat()",
        message: 'Passwords must be the same.'
    )]
    private string $newPassword = '';

    private string $newPasswordRepeat = '';

    public function getOldPassword(): string
    {
        return $this->oldPassword;
    }

    public function setOldPassword(string $oldPassword): self
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    public function getNewPassword(): string
    {
        return $this->newPassword;
    }

    public function setNewPassword(string $newPassword): self
    {
        $this->newPassword = $newPassword;

        return $this;
    }

    public function getNewPasswordRepeat(): string
    {
        return $this->newPasswordRepeat;
    }

    public function setNewPasswordRepeat(string $newPasswordRepeat): self
    {
        $this->newPasswordRepeat = $newPasswordRepeat;

        return $this;
    }
}
