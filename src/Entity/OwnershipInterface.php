<?php

namespace App\Entity;

use DateTimeInterface;

interface OwnershipInterface
{
    public function getCreatedAt(): ?DateTimeInterface;

    public function getCreatedBy(): ?User;

    public function getUpdatedAt(): ?DateTimeInterface;

    public function getUpdatedBy(): ?User;
}
