<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MinOrEmptyValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint MinOrEmpty */

        if (null === $value || '' === $value) {
            return;
        }

        if (strlen($value) < $constraint->length) {
            $this->context->buildViolation($constraint->message)
                          ->setParameter('{{ limit }}', $constraint->length)
                          ->addViolation();
        }
    }
}
