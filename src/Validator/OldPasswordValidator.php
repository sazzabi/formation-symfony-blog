<?php

namespace App\Validator;

use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class OldPasswordValidator extends ConstraintValidator
{
    public function __construct(private UserPasswordHasherInterface $userPasswordHasher)
    {
    }

    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint OldPassword */
        if (null === $value || '' === $value) {
            return;
        }
        $user = $this->context->getObject();
        if (!$user instanceof User) {
            return;
        }

        if (!$this->userPasswordHasher->isPasswordValid($user, $value)) {
            $this->context->buildViolation($constraint->message)
                          ->addViolation();
        }
    }
}
