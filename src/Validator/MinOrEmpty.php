<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class MinOrEmpty extends Constraint
{
    public int $length;

    public function __construct(
        int    $length = 6,
        string $message = '',
        array  $groups = null,
               $payload = null,
        array  $options = []
    ) {
        parent::__construct($options, $groups, $payload);
        $this->length = $length;
        $this->message = $message;
    }

    public string $message = 'Your password should be at least {{ limit }} characters.';
}
