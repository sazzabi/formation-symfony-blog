<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class OldPassword extends Constraint
{
    public string $message = 'The password is invalid.';
}
