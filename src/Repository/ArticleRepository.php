<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findForHomepage(int $page): Paginator
    {
        $query = $this->createQueryBuilder('article')
                      ->where('article.status = :status')
                      ->setParameter('status', Article::ARTICLE_STATUS_PUBLISHED)
                      ->orderBy('article.publishedAt', 'DESC')
                      ->setFirstResult(($page - 1) * Article::PAGE_SIZE)
                      ->setMaxResults(Article::PAGE_SIZE);

        return new Paginator($query);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findArticle(int $id, string $slug): ?Article
    {
        $query = $this->createQueryBuilder('article')
                      ->where('article.id = :id')
                      ->andWhere('article.slug = :slug')
                      ->setMaxResults(1)
                      ->setParameters([
                          'id' => $id,
                          'slug' => $slug,
                      ]);

        return $query->getQuery()->getOneOrNullResult();
    }
}
